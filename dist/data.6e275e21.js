// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"src/data.js":[function(require,module,exports) {
// export 
var bds = [{
  id: 0,
  titre: "20th century boys",
  editeur: "Panini",
  collection: "Panini Manga",
  serie: null,
  resume: "Menacée d'extinction à la fin du XXème siècle, l'humanité ne serait pas entrée dans le nouveau millénaire sans eux...\n ",
  type: "manga",
  etat: "abîmé",
  image: "20th-century-boys-t01.jpg",
  isbn: 9782809487572,
  annee_de_parution: 2016,
  no_tome: 1,
  prix: 16.6,
  themes: ["société", "amitié", "catastrophe"],
  auteurs: ["Naoki Urasawa"],
  like: false,
  emprunt: false
}, {
  id: 1,
  titre: "L'Ivroire du Magohamoth",
  editeur: "Soleil Productions",
  collection: "Soleil de nuit",
  serie: "Lanfeust de Troy",
  resume: "Dans le monde de Troy où chacun possède un pouvoir, Lanfeust, un apprenti forgeron peut fondre le métal. Il connaît une vie paisible jusqu'au jour où il découvre qu'au contact d'une épée, il peut posséder le pouvoir absolu ! Accompagné du vieux sage Nicolède et de ses deux filles, Cyan et Cixi, il est emporté dans un tourbillon d'aventures au cours desquelles il va se lier d'amitié avec la plus dangereuse des créatures, le troll Hébus ! De son petit village de Glinin à la ville éternelle d'Eckmül, en passant par les lointaines baronnies, suivez Lanfeust dans sa fabuleuse quête qui décidera du sort de Troy !",
  type: "bd",
  etat: "bon",
  image: "lanfeust-de-troy.jpg",
  isbn: 9782877642576,
  annee_de_parution: 1994,
  no_tome: 1,
  prix: 7.83,
  themes: ["Héroïc Fantasy"],
  auteurs: ["Christophe Arleston", "Didier Tarquin"],
  like: false,
  emprunt: false
}, {
  id: 2,
  titre: "Fairy Tail",
  editeur: "Pika",
  collection: null,
  serie: "Fairy Tail",
  resume: "Les guildes magiques sont des associations. Elles proposent différentes tâches aux magiciens, allant de la recherche d'un objet à l'attaque en règle. Lucy, une jeune fille, rêve de devenir magicienne. Un jour, elle rencontre Natsu, un magicien maîtrisant le feu, ce dernier l'invite alors à rejoindre sa guilde.",
  type: "manga",
  etat: "neuf",
  image: "fairytail.jpg",
  isbn: 9782845999145,
  annee_de_parution: 2008,
  no_tome: 1,
  prix: 3,
  themes: ["magie", "aventure", "shonen"],
  auteurs: ["Hiro Mashima"],
  like: false,
  emprunt: false
}, {
  id: 3,
  titre: "JAMES BOND T6 CASINO ROYALE",
  editeur: "Delcourt Comics",
  collection: " Contrebande",
  serie: null,
  image: "jamesbond.jpg",
  resume: "Pour sa première mission, James Bond affronte le tout-puissant banquier privé du terrorisme international, Le Chiffre. Pour achever de ruiner ce dernier et démanteler le plus grand réseau criminel qui soit, Bond doit le battre lors d'une partie de poker à haut risque au Casino Royale. La très belle Vesper Lynd, attachée au Trésor, l'accompagne afin de veiller à ce que l'agent 007 prenne soin de l'argent du gouvernement britannique qui lui sert de mise, mais rien ne va se passer comme prévu. Alors que Bond et Vesper s'efforcent d'échapper aux tentatives d'assassinat du Chiffre et de ses hommes, d'autres sentiments surgissent entre eux, ce qui ne fera que les rendre plus vulnérables...",
  type: "comics",
  etat: "bon",
  isbn: 9782413012887,
  annee_de_parution: 2020,
  no_tome: 6,
  prix: 25,
  themes: ["espionnage", "aventure", "action"],
  auteurs: ["Van Jensen", "Dennis Calero"],
  like: false,
  emprunt: false
}, {
  id: 4,
  titre: "Gretchen",
  editeur: "DUPUIS",
  collection: "",
  serie: "Zombillénium",
  resume: "Zombillénium est un parc d'attractions qui emploie des morts pour effrayer les visiteurs. Cette société (fictive) possède un capital de 2 000 000 d'euros, et se situe sur la Route du Marais Putride (D40), à Verchain-Maugrée tome 1 s'ouvre sur la tentative de fugue d'Aton Noujdmet, momie au parc Zombillénium, qui cherche à rejoindre le Caire en stop, mais il est récupéré en voiture par Francis, directeur d'exploitation du parc, et Sirius. Une dispute éclate dans le véhicule, et Francis percute accidentellement un piéton, le tuant sur le coup. Le piéton est Aurélien Zahner, tout juste quitté par sa femme. Il vient de quitter le bar-tabac qu'il tentait de braquer mais il est reconduit dans le bon chemin in extremis par Gretchen, sorcière stagiaire également employée du parc. Mordu par Francis et Blaise et transformé en démon, Aurélien est engagé comme vendeur de barbe-à-papa. À la suite d'un incident où une soudaine métamorphose provoque l'arrêt cardiaque d'une visiteuse, il est sur le point d'être « licencié » d'un coup de pieu dans le cœur, mais le directeur du parc, Behemoth, veut le tester dans le train fantôme. Sa période d'essai est un succès, malgré les manœuvres des zombies jaloux qui veulent le pousser à la faute, et il intègre définitivement Zombillénium.",
  type: "bd",
  etat: "neuf",
  image: "Gretchen Zombillénium.jpg",
  isbn: 9782800147215,
  annee_de_parution: 0,
  no_tome: 1,
  prix: 14.5,
  themes: ["bande dessinée", "fantastique", "humour"],
  auteurs: ["Arthur De Pins"],
  like: false,
  emprunt: false
}, {
  id: 5,
  titre: "La route d'Armilia",
  editeur: "Casterman",
  collection: "",
  serie: "Les Cités obscures",
  resume: "La route d'Armilia raconte le voyage de deux enfants en dirigeable, de MYLOS à ARMILIA. Le jeune Ferdinand doit apporter au professeur Pym, dans le Grand Nord, la formule capable de remettre en route les rouages du TEMPS. Le long voyage des deux enfants leur fait affronter les périls les plus divers et leur permet de survoler des villes comme BRÜSEL, Genova ou Kobenhavn, et de croiser le Vaisseau du désert du malheureux WAPPENDORF.Mais sommes-nous bien sûr que ce voyage se déroule réellement ? Ne serait-ce pas plutôt à l'intérieur d'une usine de Mylos que Ferdinand, lecteur passionné de livres interdits, a rêvé toute cette histoire ?",
  type: "bd",
  etat: "bon",
  image: "la_route_d'amilia.jpg",
  isbn: 2203343036,
  annee_de_parution: 1988,
  no_tome: 4,
  prix: 20,
  themes: ["voyage", "aventure"],
  auteurs: ["Schuiten", "Peeters"],
  like: false,
  emprunt: false
}, {
  id: 6,
  titre: "Tokyo Ghoul",
  annee_de_parution: 2015,
  editeur: "Glénat",
  collection: "Shonen",
  serie: null,
  resume: "À Tokyo, sévissent des goules, monstres cannibales se dissimulant parmi les humains pour mieux s’en nourrir. Étudiant timide, Ken Kaneki est plus intéressé par la jolie fille qui partage ses goûts pour la lecture que par ces affaires sordides, jusqu’au jour où il se fait attaquer par l’une de ces fameuses créatures. Mortellement blessé, il survit grâce à la greffe des organes de son agresseur… Remis de son opération, il réalise peu à peu qu’il est devenu incapable de se nourrir comme avant et commence à ressentir un appétit suspect envers ses congénères. C’est le début d’une descente aux enfers pour Kaneki, devenu malgré lui un hybride mi-humain, mi-goule.",
  type: "manga",
  etat: "bon",
  isbn: 9782344006474,
  no_tome: 10,
  prix: 6.6,
  themes: ["Suspense", "Fantastique"],
  auteurs: ["Sui Ishida"],
  like: false,
  emprunt: false
}, {
  id: 7,
  titre: "Comme une Ombre",
  annee_de_parution: 2018,
  auteurs: ["Pascale Legardinier,Gilles Legardinier "],
  editeur: "J'AI LU",
  collection: "J'AI LU",
  image: "Comme une Ombre-Samiha.jpg",
  resume: "Découvrer la réjouissante aventure d'un couple explosif imaginé par un vrai couple dans la vie.Partout, femmes et hommes fonts des étincelles",
  type: "manga",
  etat: "bon",
  isbn: 9782290161791,
  prix: 11.9,
  themes: ["amour"],
  like: false,
  emprunt: false
}, {
  id: 8,
  titre: "La Forêt",
  editeur: "Casterman",
  collection: null,
  serie: null,
  resume: "Ce fut un temps d'avant le vôtre... A la frontière de notre monde et des horizons inconnus... Là où le savoir de l'homme s'arrête... Là où celui des rêveurs commence... Un temps que seuls les magiciens peuvent comprendre. ",
  type: "bd",
  etat: "neuf",
  image: "La-Foret.jpg",
  isbn: 9782203391598,
  annee_de_parution: 2007,
  no_tome: 1,
  prix: 15,
  themes: ["Fantastique", "Féérique"],
  auteurs: ["Vincent Perez", "Tiburge Oger"],
  like: false,
  emprunt: false
}, {
  id: 9,
  isbn: 2791092111682,
  titre: "Le yéti qui avait perdu l'appétit",
  image: "Enola.jpg",
  annee_de_parution: 2008,
  auteurs: ["Joris Chamblain", "Lucile Thibaudier"],
  editeur: "Editions de la Gouttiére",
  collection: "jeunesse",
  serie: "Enola & les animaux extraordinaires",
  no_tome: 4,
  prix: 10.7,
  resume: "Caché u coeur d'histoire Naturelle se trouve le cabinet d'Enola, une vétérinaire hors du commun.\nSa spécilalité?\nLes animaux des contes et légendes!",
  type: "bd",
  themes: ["amitié", "animaux", "fantastique"],
  etat: "neuf"
}, {
  id: 10,
  titre: "Jusqu'au dernier",
  editeur: "Bamboo",
  collection: "GRAND ANGLE",
  serie: null,
  resume: "L’époque des cow-boys tire à sa fin. Bientôt, ce sont les trains qui mèneront les vaches jusqu'aux abattoirs de Chicago. Accompagné de Benett, un jeune simplet de 20 ans, Russell a décidé de raccrocher ses éperons pour devenir fermier dans le Montana.",
  type: "bd",
  etat: "neuf",
  isbn: 9782818967003,
  annee_de_parution: 2019,
  no_tome: 1,
  prix: 17.9,
  themes: ["western"],
  auteurs: ["Jérôme Félix", "Paul Gastine"],
  like: false,
  emprunt: false
}, {
  id: 11,
  titre: "Un amour de chat",
  editeur: "Soleil",
  collection: "Soleil Manga",
  serie: "Plum",
  annee_de_parution: 2009,
  auteurs: ["Natsumi Hoshino"],
  image: "plum.jpg",
  no_tome: 2,
  prix: 8.25,
  resume: "Plum, jeune chatte pleine d’amour, ne voulait qu’une vie paisible à être dorlotée par son maître, Taku. Sa vie a basculé depuis l’arrivée de Flocon, un chaton malicieux qui en fait voir de toutes les couleurs à Plum. Mais le temps passe, et les deux félins s’habituent à la cohabitation, bien que les bêtises ne manquent pas !",
  themes: ["animaux", "amitié", "aventure", "chats"],
  isbn: 9782302038417,
  type: "manga",
  etat: "neuf"
}, {
  id: 12,
  titre: "Catwoman: La main au collet",
  editeur: "Urban Comics",
  collection: "DC Renaissance",
  serie: null,
  resume: "Les rues de Gotham sont inquiétantes, \n mais ce qui rampe sous ses rues pavées l'est encore davantage...  \n  Engagée dans une guerre contre les pires gangsters de la ville, \n Catwoman met une fois de plus ses plus proches alliés en danger.  \n Pour sauver l'un d'entre eux, elle devra s'aventurer dans les sinistres labyrinthes de la Cité et affronter ce qui, \n jusqu'alors, demeurait méconnu de tous.",
  type: "comics",
  etat: "neuf",
  isbn: 9782365776592,
  annee_de_parution: 2015,
  no_tome: 4,
  prix: 19,
  themes: ["batman", "comics", "thriller"],
  auteurs: ["Ann Nocenti", "Sandoval Rafa"],
  like: false,
  emprunt: false
}, {
  id: 13,
  titre: "L'enfant des étoiles",
  editeur: "Le Lombard",
  collection: null,
  serie: "Thorgal",
  resume: "Le secret des origines de Thorgal ? Tout commence par la découverte d'un bébé sur un mystérieux radeau par un chef viking. A l'adolescence, Thorgal désire connaître son passé. Un dieu intercédera en sa faveur pour percer le secret de sa naissance... Un album fondateur composé de trois histoires qui dévoilent les origines de Thorgal.",
  type: "bd",
  etat: "bon",
  image: "thorgal7.jpg",
  isbn: 2803604604,
  annee_de_parution: 1984,
  no_tome: 7,
  prix: 20.09,
  themes: ["Médiéval", "fantastique", "Aventure"],
  auteurs: ["Jean Van Hamme", "Grzegorz Rosinski"],
  like: false,
  emprunt: false
}, {
  id: 14,
  titre: "Twilight Fascination volume 1",
  editeur: "Pika Edition",
  collection: null,
  serie: "Twilight",
  resume: "Adaptation graphique de la première partie du roman Fascination",
  type: "manga",
  etat: "bon",
  isbn: 9782811602451,
  annee_de_parution: 2010,
  no_tome: 1,
  prix: 12.9,
  themes: ["romance", "vampire", "young adulte"],
  auteurs: ["Stephanie Meyer", "Young Kim"],
  image: "twilight.jpg"
}];
},{}],"node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "50424" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["node_modules/parcel-bundler/src/builtins/hmr-runtime.js","src/data.js"], null)
//# sourceMappingURL=/data.6e275e21.js.map